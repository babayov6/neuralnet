import random
import numpy as np


def is_float(val):
    try:
        float(val)
        return True
    except:
        return False


def extract(path):
    with open(path, 'r') as f:
        lines = f.readlines()
    data = []
    for line in lines:
        values = line.strip().split(',')
        values = [float(val) for val in values if is_float(val)]
        data.append(values)
    return data


def add_random_zeroes(array, zero_percent):
    for i in range(0, len(array)):
        rand = random.uniform(0, 1)
        if rand < zero_percent:
            array[i] = 0
    return np.array(array)
