import Layer
import utils
import time
import numpy as np


class Network:
    def __init__(self, layers_size, learning_rate, zero_percent):
        self._layers_size = layers_size
        self.learning_rate = learning_rate
        self.zero_percent = zero_percent
        self.layers = [Layer.Layer(layers_size[i - 1], layers_size[i]) for i in range(1, len(layers_size))]

    def test(self, test_path):
        answers = []
        test_data = utils.extract(test_path)
        for line in test_data:
            line.append(-1)
            for layer in self.layers:
                line = layer.calculate(line)

            answers.append(str(np.argmax(line) + 1))
        return answers

    def train(self, train_path, validate_path):
        net_snapshot = None
        start = time.time()
        validate = utils.extract(validate_path)
        data = utils.extract(train_path)
        epoc_no = 0
        max_rate = 0
        downward_times = 1
        train_scores = []
        validate_scores = []
        while True:
            epoc_no += 1
            print(f'Starting epoc number: [{epoc_no}]')
            train_success_rate = self.epoc(data)
            success = self.validate(validate)
            print(f'train: {train_success_rate}, validation: {success}. time so far: {time.time() - start}')
            train_scores.append(train_success_rate)
            validate_scores.append(success)
            if success > max_rate:
                max_rate = success
                net_snapshot = self.snapshot()
                downward_times = 0
            else:
                downward_times += 1
                if downward_times > 6:
                    print(f'Completed training. Max rate is: [{max_rate * 100.0}%]')
                    return net_snapshot, max_rate, train_scores, validate_scores

    def snapshot(self):
        net = Network(self._layers_size, self.learning_rate, self.zero_percent)
        net.layers = [layer.copy() for layer in self.layers]
        return net

    def epoc(self, data):
        np.random.shuffle(data)
        success = 0
        for i, line in enumerate(data):
            success += self.train_line(line)
            i += 1
            if i % 500 == 0:
                print(f'Finished {i} examples')
        return success / len(data)

    def validate(self, data):
        correct = 0
        for line in data:
            target, line_data = line[0], line[1:]
            line_data.append(-1)
            for layer in self.layers:
                line_data = layer.calculate(line_data)

            if np.argmax(line_data) + 1 == target:
                correct += 1
        return correct / len(data)

    def train_line(self, line):
        target, line_data = line[0], line[1:]
        line_data_zeroes = utils.add_random_zeroes(line_data, self.zero_percent)
        result = self.insert_line(line_data_zeroes)
        self.update_weights(target, result)
        return 1 if np.argmax(result) + 1 == target else 0

    def insert_line(self, line):
        line = np.insert(line, len(line), -1)
        for layer in self.layers:
            line = layer.calculate(line)
        return line

    def update_weights(self, target, result):
        error = [1 - result[i] if target == i + 1 else 0 - result[i] for i in range(len(result))]
        error = np.reshape(np.array(error), (1, len(error)))
        for i in range(1, len(self.layers)):
            next_err = self.layers[len(self.layers) - i].calculate_error(error)
            self.layers[len(self.layers) - i].update_weights(error, self.learning_rate)
            error = next_err
