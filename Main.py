import pickle

from Network import Network

import json
import pprint
import argparse
import random
import numpy as np
from matplotlib import pyplot as plt

random.seed(1024)
np.random.seed(1024)


def main():
    with open('config.json', 'r') as f:
        params = json.load(f)

    pprint.pprint(params)
    args = parse_arguments()
    if args.test:
        test_net(params, args.output)
        return

    network = Network(params['layers'], params['learning_rate'], params['zero_percent'])
    trained_network, max_rate, train_scores, validate_scores = \
        network.train(params['train_path'], params['validate_path'])

    plot_results(train_scores, validate_scores)
    save_net(trained_network, params['trained_network'])


def save_net(trained_network, net_file):
    with open(net_file, 'wb') as f:
        pickle.dump(trained_network, f)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Neural Networks')
    parser.add_argument('--test', action="store_true", help='run test on trained network')
    parser.add_argument('--output', type=str, help='Path to the output test file')

    args = parser.parse_args()
    if args.test and args.output is None:
        parser.error('When executing --test option, --output are required.')

    return args


def test_net(params, test_output_results_file):
    with open(params['trained_network'], 'rb') as net_file:
        trained_network = pickle.load(net_file)

    answers = trained_network.test(params['test_path'])
    with open(test_output_results_file, 'w') as f:
        f.write('\n'.join(answers))


def plot_results(train_scores, validate_scores):
    plt.plot([i for i in range(len(train_scores))], train_scores, label='train')
    plt.plot([i for i in range(len(train_scores))], validate_scores, label='validation')
    plt.xlabel('epoc number')
    plt.ylabel('success rate')
    plt.legend()
    plt.savefig("figure.jpg")


if __name__ == "__main__":
    main()
