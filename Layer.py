import numpy as np
import math


class Layer(object):
    def __init__(self, *args):
        self._last_input = None
        if len(args) == 2:
            self._init_from_dimensions(*args)
            return
        self._init_from_copy(*args)

    def _init_from_copy(self, weighted_matrix, last_input, no_neurons):
        self._weights_matrix = weighted_matrix
        self._last_input = last_input
        self._no_neurons = no_neurons

    def _init_from_dimensions(self, no_neurons, next_layer_size):
        self._no_neurons = no_neurons
        self._weights_matrix = np.random.random_integers(-1000, 1000, (no_neurons + 1, next_layer_size + 1)) / 100000
        self._weights_matrix[no_neurons, :] = 0

    @staticmethod
    def _relhu(val):
        return max(0, val)

    @staticmethod
    def _relhu_deriv(val):
        return 1 if val > 0 else 0

    def calculate(self, layer_inputs):
        assert len(layer_inputs) == self._no_neurons + 1
        layer_inputs[-1] = -1
        layer_inputs = np.reshape(layer_inputs, (1, len(layer_inputs)))
        self._last_input = layer_inputs
        calculated_outputs = np.dot(layer_inputs, self._weights_matrix)
        return [self._relhu(val) for val in calculated_outputs[0, :]]

    def calculate_error(self, next_layer_error):
        weighted_error = np.dot(self._weights_matrix, np.transpose(next_layer_error))
        deri_value = np.array([self._relhu_deriv(val) for val in self._last_input[0, :]])
        result = np.multiply(deri_value, np.transpose(weighted_error))
        return result

    def update_weights(self, next_layer_error, learning_rate):
        func_input = np.reshape([self._relhu(val) for val in self._last_input[0, :]], (self._last_input.shape[1], 1))
        val = learning_rate * np.dot(func_input, next_layer_error)
        z = self._weights_matrix + val
        self._weights_matrix += val

    def copy(self):
        return Layer(self._weights_matrix, self._last_input, self._no_neurons)
